FROM mdland/pppweb:python
WORKDIR /code
COPY . .
ENV PYTHONUNBUFFERED=1
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
import json
import base64
from pyDes import triple_des, ECB, PAD_PKCS5
from PhmWeb.common import get_PHM_db
from PhmWeb.dbmodel.DbPhmOrgan import DbPhmOrgan
from PhmWeb.dbmodel.DbPhmUser import DbPhmUser
from PhmWeb.lang.lang_chinese import lang_chinese
from PhmWeb.lang.lang_english import lang_english
from PhmWeb.lang.lang_spanish import lang_spanish


class SessionManager:
    def __init__(self, reqeust):
        self._req = reqeust
        self._usession = SessionManager.get_session(reqeust)
        self._online_object_id = None

    @property
    def session(self):
        return self._usession

    def get_lang_dict(self):
        usession = self.session
        if usession.Language == 'Chinese':
            return lang_chinese
        elif usession.Language == 'Spanish':
            return lang_spanish
        else:
            return lang_english


    @property
    def online_object_id(self):
        if self._online_object_id is None:
            if self._usession is not None and self._usession.OnlineId > 0:
                zw = get_PHM_db()
                obj = zw['phm_log_Login'].find_one({'OnlineId': self._usession.OnlineId})
                self._online_object_id = obj['_id'] if obj is not None else ''
        return self._online_object_id

    @staticmethod
    def get_session(request):
        # http session is ok
        uname = request.session.get('username', None)
        language = request.session.get('language', None)
        if language is not None:
            usession = UserSession()
            usession.UserName = uname
            usession.Email = request.session.get('email', '')
            usession.OnlineId = request.session.get('onlineid', -1)
            usession.PatientID = request.session.get('patientid', -1)
            usession.Language = request.session.get('language', '')
            return usession

        # recover from cookie
        session_cookie = request.COOKIES.get('ppp_ussession', '')
        if session_cookie:
            print('use cookie...')
            session_cookie = SessionManager.sso_decrypt(session_cookie, 'PhCookie')
            if session_cookie is not None:
                usession = json.loads(session_cookie, object_hook=user_session_handle)
                save_http_session(request, usession)
                return usession

        # this is an empty session
        usessoin = UserSession()
        return usessoin

    def change_session(self, session_key, session_value):
        self._req.session[session_key] = session_value


    # DES加密
    @staticmethod
    def sso_encryption(data, key):
        # string 转 bytes
        key = key.encode(encoding='utf-8')
        print('len(key)={0}'.format(len(key)))
        # key如果不足16位在后面补0 改成了重复 才能跟 iClinic一致
        while len(key) < 16:
            key += key

        k = triple_des(key, ECB,  padmode=PAD_PKCS5)
        d = k.encrypt(data)
        d = base64.b64encode(d)
        # bytes 转 string
        d = d.decode(encoding='utf-8')
        print(f"SessioniManager.sso_encryption={d}" )
        return d

    # 解密
    @staticmethod
    def sso_decrypt(cipher, key):
        # string 转 bytes
        key = key.encode(encoding='utf-8')

        # key如果不足16位在后面补0--改成了重复 iClinic一致
        while len(key) < 16:
            key += key
        cipher = cipher.replace(" ", "+")    # cipher作为url的一部分通过浏览器传递到服务端时，"+"会被替换为空格，此处需要还原回来
        cipher = base64.b64decode(cipher)

        k = triple_des(key, ECB, padmode=PAD_PKCS5)
        result = k.decrypt(cipher)

        # bytes 转 string
        try:
            result = result.decode(encoding='utf-8')
        except:
            print('sso_decrypt error')
            return None
        print("Decrypted: %r" % result)
        return result

    def response_cookie(self, response, pat):
        # save session
        cur_session = UserSession()
        cur_session.Email = f"{pat.get('Email')}"
        cur_session.UserName = 'P{0}'.format(pat['PatientID'])
        cur_session.PatientID = pat['PatientID']
        save_http_session(self._req, cur_session)

        # response = HttpResponse(json.dumps(response_data), content_type="application/json")
        # response = HttpResponseRedirect('/p/results/')
        print('response_cookie.cur_session={0}'.format(cur_session))
        cur_session = json.dumps(cur_session, default=lambda x: x.__dict__)
        cur_session = SessionManager.sso_encryption(cur_session, 'PhCookie')
        response.set_cookie('ppp_ussession', cur_session, expires=60 * 60 * 24)
        return response


def user_session_handle(d):
    user_session = UserSession()
    user_session.Email = d['Email'] if 'Email' in d else ''
    user_session.UserName = d['UserName'] if 'UserName' in d else ''
    user_session.OnlineId = d['OnlineId'] if 'OnlineId' in d else -1
    user_session.PatientID = d['PatientID'] if 'PatientID' in d else -1
    return user_session


def save_http_session(request, usession):
    request.session['patientid'] = usession.PatientID
    request.session['username'] = usession.UserName
    request.session['onlineid'] = usession.OnlineId


class UserSession:
    def __init__(self):
        self.PatientID = -1
        self.UserName = ''
        self.Email = ''
        self.OnlineId = 0
        self.Language = "" # English, Spanish, Chinese

    def __str__(self):
        mc = []
        if self.PatientID > 0:
            mc.append('PatientID={0}'.format(self.PatientID))
        if self.UserName != '':
            mc.append('UserName={0}'.format(self.UserName))
        if self.Email != '':
            mc.append('Email={0}'.format(self.Email))
        if self.OnlineId > 0:
            mc.append('OnlineId={0}'.format(self.OnlineId))
        return ','.join(mc)

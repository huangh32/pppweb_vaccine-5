import threading, datetime
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.common import Utils


class DbPhmLogCareGap:
    _instance_lock = threading.Lock()

    def __init__(self):
        pass

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmLogCareGap, "_instance"):
            with DbPhmLogCareGap._instance_lock:  # for thread safety
                if not hasattr(DbPhmLogCareGap, "_instance"):
                    DbPhmLogCareGap._instance = DbPhmLogCareGap(*args, **kwargs)
        return DbPhmLogCareGap._instance

    def add_qm_log(self, patient_uid, request):
        log = dict()
        pathinfo = "" + request.META['PATH_INFO']
        log['PatientUID'] = patient_uid
        log['Channel'] = 'qm'
        log['VisitDate'] = datetime.datetime.now()
        is_first_get = True
        for item in request.GET:
            item_value = request.GET[item]
            log[item] = item_value
            pathinfo += '?' if is_first_get else "&"
            pathinfo += "{0}={1}".format(item, item_value)
            is_first_get = False
        log['PathInfo'] = pathinfo

        for item in request.POST:
            if item in log:
                log['Post_' + str(item)] = request.POST[item]
            else:
                log[item] = request.POST[item]

        # change action -->Action, command -- > Command
        change_prop = [('action', 'Action'), ('command', 'Command'), ('clinicid', 'ClinicID'), ('patientid', 'PatientID')]
        for item in change_prop:
            if item[0] not in log:
                continue
            log[item[1]] = log[item[0]]
            log.pop(item[0])

        if 'ClinicID' in log:
            log['ClinicID'] = Utils.to_numeric(log['ClinicID'])

        zw = get_PHM_db()
        zw['phm_log_CareGap'].insert(log)

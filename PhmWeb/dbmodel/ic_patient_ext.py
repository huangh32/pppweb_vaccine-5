import datetime, uuid
from PhmWeb.common import RequestUtils, DateUtils
from PhmWeb.utils.logger import Logger
from PhmWeb.utils.dbconfig import sql_ec_db


class IcPatientExt:
    def __init__(self):
        self._ec_db = sql_ec_db(as_dict=True)

    def save_patient_race(self, patientID, clinicID, race_codes):
        if race_codes is None:
            return

        race_codes = race_codes.split(',')
        sql = f"delete from Patient_Race where PatientID='{patientID}' "
        if len(race_codes) > 0:
            race_codes = [x.strip() for x in race_codes if x != '']
            sql += " and RaceCode not in ('{0}');".format("','".join(race_codes))
            for code in race_codes:
                sql += f""" if not exists (select 1 from Patient_Race where PatientID='{patientID}' and RaceCode='{code}') 
                            insert into Patient_Race(PatientID,ClinicID, RaceCode) values('{patientID}', {clinicID}, '{code}');"""
        self._ec_db.excute_update(sql)

    def save_patient_ethnicity(self, patientID, clinicID, ethnicity_codes):
        if ethnicity_codes is None:
            return

        codes = ethnicity_codes.split(',')
        sql = f"delete from Patient_Ethnicity where PatientID='{patientID}' "
        if len(codes) > 0:
            codes = [x.strip() for x in codes if x != '']
            sql += " and EthnicityCode not in ('{0}');".format("','".join(codes))
            for code in codes:
                sql += f""" if not exists (select 1 from Patient_Ethnicity where PatientID='{patientID}' and EthnicityCode='{code}') 
                            insert into Patient_Ethnicity(PatientID,ClinicID, EthnicityCode) values('{patientID}', {clinicID}, '{code}');"""
        print(sql)
        self._ec_db.excute_update(sql)


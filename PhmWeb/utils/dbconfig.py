from pymongo import MongoClient
import pymssql
import urllib
import xml.dom.minidom as xmldom
import redis
from PhmWeb.biz.environment_variables import EnvironmentVariables


def get_DW_db():
    client = MongoClient(EnvironmentVariables.MDLAND_DC_MONGODB, serverSelectionTimeoutMS=500)
    db = client.DW_Prod
    return db


# for
# phmdb = get_PHM_db();
def get_PHM_db():
    client = MongoClient(EnvironmentVariables.MDLAND_DC_MONGODB, serverSelectionTimeoutMS=500)
    db = client.zw
    return db


# sqlserver
class sql_ec_db():
    conn = None

    def __init__(self, as_dict=False):
        self.as_dict = as_dict
        self.conn = pymssql.connect(server=EnvironmentVariables.ECLINIC_DB67_IP, user='ecuser', password='1234', database=EnvironmentVariables.ECLINIC_DB67_NAME)

    def excute(self, sql, commit=True):
        cursor = self.conn.cursor(as_dict=self.as_dict)

        cursor.execute(sql)
        rows = cursor.fetchall()
        if commit:
            self.conn.commit()

        cursor.close()
        return rows

    def excute_select(self, sql):
        return self.excute(sql, commit=False)

    def excute_update(self, sql, fetchall=False):
        cursor = self.conn.cursor(as_dict=self.as_dict)
        cursor.execute(sql)
        if fetchall:
            res = cursor.fetchall()
        else:
            res = cursor.rowcount
        self.conn.commit()
        cursor.close()
        return res

    def close_conn(self):
        self.conn.close()

    def query_first_row_to_dict(self, sql):
        res_list = self.fetch_sql_dict_list(sql)
        return res_list[0] if len(res_list) > 0 else dict()

    def fetch_sql_dict_list(self, sql):
        cursor = self.conn.cursor(as_dict=True)
        cursor.execute(sql)
        rows = cursor.fetchall()
        cursor.close()
        return rows


def get_redis():
    r = redis.Redis(host=EnvironmentVariables.REDIS_HOST, port=6379, db=3)
    return r



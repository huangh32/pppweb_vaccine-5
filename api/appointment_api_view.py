import datetime

from somos import s2_sites_view
from somos.biz.slot_utils import SlotUtils
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.dbmodel.DbPhmLogRestAPI import DbPhmLogRestAPI

'''
http://192.168.168.105:8158/p/api/apt/?AppKey=MDLandSOMOSVIP20&Action=ValidateSlotBeforeCommit&ClinicID=3553&LocationID=1368&BookTime=02/01/2012%2023:11:00
http://192.168.168.105:8158/p/api/apt/
parameter: post or get 
    AppKey=MDLandSOMOSVIP20
    Action=ValidateSlotBeforeCommit
    ClinicID=3553 # SOMOS
    LocationID=int #
    BookTime=02/01/2012 23:11:00

return 
    {'ResultCode': int}
    ResultCode: 0, pass
    ResultCode: 200, invalid AppKey
    ResultCode: 201, not valid request
    ResultCode: 1, not valid because of vacation days
    ResultCode: 2, not valid because of having reached the upper limit of the day
    ResultCode: 3, not valid because the slot is occupied by others

'''
@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, "Action")
    app_key = RequestUtils.get_string(request, 'AppKey')

    res_dict = dict({'ResultCode': 201})
    if app_key != 'MDLandSOMOSVIP20':
        res_dict = dict({'ResultCode': 200, 'Message': 'Invalid AppKEY'})
        return JsonResponse(res_dict)

    if action == 'ValidateSlotBeforeCommit':
        location_id = RequestUtils.get_long(request, 'LocationID')
        book_time_str = RequestUtils.get_string(request, 'BookTime')
        book_time = datetime.datetime.strptime(book_time_str, "%m/%d/%Y %H:%M:%S")
        res_dict['ResultCode'] = SlotUtils.is_able_to_commit_appointment(location_id, book_time)

    if action == 'GetSiteWithSlot':
        res_dict['Result'] = s2_sites_view.get_site_with_slot(request, True)
        res_dict['ResultCode'] = 0

    res_dict['LogType'] = 'VIPH_API'

    res_log = dict()
    res_log.update(res_dict)
    DbPhmLogRestAPI.instance().add_log(restlog=res_log, request=request)

    return JsonResponse(res_dict)


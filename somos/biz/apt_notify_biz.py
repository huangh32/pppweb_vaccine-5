from datetime import datetime, timedelta
from PhmWeb.biz.aws_rabbit_mq import AwsRabbitMQ
from PhmWeb.common import DictUtils, DateUtils
from PhmWeb.utils.dbconfig import sql_ec_db


class AptNotifyBiz:
    def __init__(self):
        pass

    '''
    [Patient Name], Your COVID vaccine appointment is confirmed at:
    [Site Name]
    [Site Address]
    Date: [Appointment Date]
    Time: [Appointment Time]
    Arrive 15 minutes early.
    Before you go:
    * Bring your government-issued photo ID, health insurance card, and proof of employment or license number if you are a healthcare or essential worker.
    * Bring your Agency-issued Employee ID and health insurance card if you are city employee. 
    '''
    def send_appointment_sms_notify(self, apt_info):
        if apt_info is None:
            return
        clinic_id = apt_info['ClinicID']
        if clinic_id != 3553:
            return
        sms_status = DictUtils.get_int_value(apt_info, 'SMSStatus')
        if sms_status > 0:
            return

        pat_name = apt_info["PatientName"]
        location_name = apt_info['LocationName']

        location_addr = apt_info['LocationAddress']
        location_city = apt_info['LocationCity']
        location_state = apt_info['LocationState']
        location_zip = apt_info['LocationZip']
        location_full_addr = f"{ location_addr }, { location_city }, { location_state }, { location_zip }"

        apt_dt = datetime.strptime(str(apt_info['AppointmentDate']), '%Y-%m-%d %H:%M:%S')
        apt_id = apt_info['AppointmentID']
        pat_phone = apt_info['PatientMobilePhone']

        location_id = apt_info['LocationID']
        ec_db = sql_ec_db(as_dict=True)
        #if False and location_id == 1396 and apt_info['AppointmentDate'].find('2021-02-07') == 0:
        #    sms_content = f"""{pat_name}, Please disregard the previous text. your appointment tomorrow 1/31 has been cancelled. Your COVID vaccine appointment will be rescheduled at:{location_name}, {location_full_addr}  Date: {apt_dt}, Arrive 15 minutes early."""

        sms_content = f"""{pat_name}, Your COVID vaccine appointment is confirmed at:{location_name}, {location_full_addr}  Date: {apt_dt}, """
        if location_id in [1412]:
            sms_content += 'Please arrive at the time of your appointment. If you are more than 15 minutes late, your appointment may not be honored, and you will not receive a vaccine.'
        else:
            sms_content += 'Arrive 15 minutes early.'
        sms_content += 'Pls visit somosvaccinations.com for more information'

        mq_biz = AwsRabbitMQ()
        other_info = dict({'ClinicID': clinic_id, 'PatientID': apt_info.get('PatientID')})
        mq_biz.public_apt_sms(clinic_id=clinic_id, phone_number=pat_phone, sms_content=sms_content, appointment_id=apt_id, other_info=other_info)
        mq_biz.close()

        sql_update = "update appointment set SMSStatus=1 where ID='{0}'".format(apt_id)

        ec_db.excute_update(sql_update)
        ec_db.close_conn()


    def load_appointment(self, app_id, clinic_id):
        if app_id is None:
            return None

        sql = """select case when a.PatientID = 0 then a.PatientName else rtrim(p.PatientLastName) end PatientName, a.ClinicID, a.PatientID, 
                     l.OLID as LocationID ,l.Name LocationName, format(a.BeginDateTime, 'yyyy-MM-dd HH:mm:ss') AppointmentDate, l.Address LocationAddress, 
                     l.City LocationCity, l.State LocationState, l.Zip LocationZip, l.Phone LocationPhone
                     ,p.PatientMobilePhone,a.ID as AppointmentID, a.SMSStatus, p.PatientEmail
                                      from Appointment a 
                                      left join Patient p on a.PatientID = p.PatientID
                                      left join Ref_ClinicOfficeLocation l on a.LocationID = l.OLID and a.ClinicID = l.ClinicID
                                      where a.ID='{0}' and a.ClinicID='{1}'
    
        """.format(app_id, clinic_id)
        ec_db = sql_ec_db(as_dict=True)
        ret = ec_db.excute_select(sql)
        ec_db.close_conn()

        if len(ret) > 0:
            return ret[0]
        return None


    def print_appointment_html(self, apt_id, clinic_id):
        apt_info = self.load_appointment(apt_id, clinic_id)
        if not apt_info:
            return ''
        print(f'apt_info={apt_info}')
        ss = ''

        pat_name = apt_info["PatientName"]
        location_name = apt_info['LocationName']

        location_addr = apt_info['LocationAddress']
        location_city = apt_info['LocationCity']
        location_state = apt_info['LocationState']
        location_zip = apt_info['LocationZip']
        location_full_addr = f"{ location_addr }, { location_city }, { location_state }, { location_zip }"

        apt_dt = datetime.strptime(str(apt_info['AppointmentDate']), '%Y-%m-%d %H:%M:%S')
        ss = f"""{pat_name}, We have found that you already have COVID vaccine appointment at:<br/><br/>
                        Location:    {location_name}, {location_full_addr}   <br/><br/>
                        Date: {apt_dt} <br/> <br/>
                        Please arrive 15 minutes early at that time, and do not need make multiple appointments."""

        return ss

import datetime, os
from urllib import parse
from io import BytesIO
from hashlib import md5
import requests, json
from django.templatetags.static import static
from django.core.files.storage import default_storage
from PhmWeb.settings import BASE_DIR
from PhmWeb.utils.dbconfig import get_PHM_db
from somos.biz.gmap_mongo_cache import GmapMongoCache


class GmapUtils:

    @staticmethod
    def get_url_safe_full_addr(addr, city, state, zip):
        mapstring = ""
        if addr:
            mapstring += parse.quote_plus(addr.strip())
            mapstring += "+"
        if city:
            mapstring += parse.quote_plus(city.strip())
            mapstring += "+"
        if state:
            mapstring += parse.quote_plus(state.strip())
            mapstring += "+"
        if zip:
            mapstring += parse.quote_plus(str(zip).strip())
        return mapstring

    @staticmethod
    def gmap_img(addr, city, state, zip, key):
        return "https://maps.googleapis.com/maps/api/staticmap?center={map}&size=640x640&key={key}".format(
            map=GmapUtils.get_url_safe_full_addr(addr, city, state, zip),
            key=key)

    @staticmethod
    def gmap_img_from_cache(addr, city, state, zip, key):
        map_hash_key = md5(GmapUtils.get_url_safe_full_addr(addr, city, state, zip).encode()).hexdigest() + "_%s" % datetime.datetime.now().year
        relfilename = "images/maps_cache/" + map_hash_key + ".png"
        fullfilename = os.path.normpath(BASE_DIR + "/static/" + relfilename)

        if not default_storage.exists(fullfilename):
            gmap_cache = GmapMongoCache()
            # get file from mongodb
            is_mongo_cached = gmap_cache.write_to_cache_file_if_exists(map_hash_key, fullfilename)

            if not is_mongo_cached:
                img = GmapUtils.gmap_img(addr, city, state, zip, key)
                response = requests.get(img)
                if not response.status_code == 200:
                    url = "https://via.placeholder.com/640x640?text=Map+Temporarily+Unavailable"
                    return url
                filelike = BytesIO(response.content)
                default_storage.save(fullfilename, filelike)

                # save to mongodb
                gmap_cache.save_file_to_mongo(file_path=fullfilename, filename=relfilename, map_hash_key=map_hash_key)

        url = static(relfilename)
        return url

    @staticmethod
    def distance_matrix(from_addr, dest_list, key):

        url = 'https://maps.googleapis.com/maps/api/distancematrix/json?'
        url_full = url + 'origins=' + from_addr + '&destinations=' + "|".join(dest_list) + '&key=' + key + "&units=imperial"
        print(url_full)
        r = requests.get(url_full)
        x = r.json()
        print(x)
        return x

    @staticmethod
    def distance_matrix_from_cache(from_addr, dest_list, key):
        zw = get_PHM_db()
        query_id = md5((from_addr + ".".join(sorted(dest_list))).encode()).hexdigest()
        matrix_cache = zw["gmaps_distance_matrix_cache"]
        exists = matrix_cache.find_one(
            dict(
                query_id=query_id,
                expires={"$gt": datetime.datetime.now()}
            )
        )
        if not exists:
            response = GmapUtils.distance_matrix(from_addr, dest_list, key)
            print(response)
            if response.get("status") == "OK":
                matrix_cache.insert(
                    dict(
                        query_id=query_id,
                        created=datetime.datetime.now(),
                        expires=datetime.datetime.now() + datetime.timedelta(days=365),
                        response=response,
                        dest_list=dest_list,
                        from_addr=from_addr,
                        key=key
                    )
                )
        else:
            response = exists["response"]
        return response

import datetime, os, binascii
from PhmWeb.biz.aws_ses import AwsSES
from PhmWeb.utils.dbconfig import get_PHM_db


class EmailVerifySes:
    def __init__(self):
        self._zw = get_PHM_db()

    def find_effective_token_link(self, clinic_id, patient_id):
        dt_one_hour_ago = datetime.datetime.now() - datetime.timedelta(hours=1)
        vo = self._zw['vac_ses_EmailVerity'].find_one({'ClinicID': clinic_id, 'PatientID': patient_id,
                                                       'Status': 0, 'GenerateDate': {'$gt': dt_one_hour_ago} })
        return vo

    def send_verify_email(self, clinic_id, patient_id, email_addr):
        vo = self.find_effective_token_link(clinic_id, patient_id)
        if vo:
            return vo
        link, token = self.generate_verify_link(clinic_id)
        subject, content = self.generate_verify_content(link)
        res = AwsSES().send_email(reciptent=email_addr, subject=subject, content=content)
        po = res
        po['GenerateDate'] = datetime.datetime.now()
        po['ClinicID'] = clinic_id
        po['PatientID'] = patient_id
        po['Link'] = link
        po['Token'] = token
        po['Email'] = email_addr
        po['Status'] = int(0)
        self._zw['vac_ses_EmailVerity'].insert_one(po)

    def generate_verify_link(self, clinic_id):
        token = binascii.hexlify(os.urandom(20)).decode()
        domain = 'https://rendrcare.mdland.com' if clinic_id == 3552 else 'https://somosvaccination.mdland.com'
        link = f"{domain}/p/emailverify/?confirmation_token={token}"  # VaBfoyqxJYLXYfJAVkfW
        return link, token

    def generate_verify_content(self, link):
        subject = 'Please verify your email address'
        content = f"""
           <div class="x_body" style="background-color:#E5E5E5; margin:0; padding:0; background-color:#E5E5E5">
            <div style="display:none; max-height:0px; overflow:hidden">Thanks for creating an account on SOMOS vaccination site. Please verify your email address.</div>
            <meta content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0 ">
            <meta name="format-detection" content="telephone=no">
            <style type="text/css">
            <!--
            .rps_2a0a body{{margin:0;padding:0;background-color:#E5E5E5}}
            .rps_2a0a img{{border:0!important;outline:none!important}}
            .rps_2a0a p{{margin:0px!important;padding:0px!important}}
            .rps_2a0a table{{border-collapse:collapse}}
            .rps_2a0a a[x-apple-data-detectors], .rps_2a0a #x_MessageViewBody a{{color:inherit;text-decoration:none;
                font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}}
            @media only screen and (max-width: 599px) {{
            .rps_2a0a table.x_em_table_wrapper{{width:100%!important;max-width:100%!important}}
            .rps_2a0a .x_img_wrapper_1{{max-width:80%!important}}
            .rps_2a0a .x_em_padding{{padding-left:22px!important;padding-right:22px!important}}
            .rps_2a0a .x_td_width{{max-width:100px!important}}
                }}
            -->
            </style>
            
            <table id="x_table-beige-wrapper" class="x_em_table_wrapper" style="max-width:650px; border-collapse:collapse" width="650" height="0" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFAE5" align="center">
            <tbody>
             <tr><td class="x_em_padding" style="padding-top:42px; padding-bottom:48px; padding-left:60px" align="left"> 
               </td></tr>
             <tr><td class="x_em_padding" style="font-family:'Work Sans',Arial,sans-serif; font-size:16px; font-style:normal; font-weight:400; line-height:26px; letter-spacing:0px;padding-left:60px" align="left"> Please verify your Email Address.</td></tr>
             <tr><td class="x_em_padding" style="font-family:'Work Sans',Arial,sans-serif; font-size:16px; font-style:normal; font-weight:400; line-height:26px; letter-spacing:0px; padding:16px 60px 24px 60px" align="left">
                To secure your data, just verify your email address to complete the setup process. </td></tr>
             <tr><td style="padding-bottom:24px" align="center"><a href="{link}" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable">
              <img data-imagetype="External" blockedimagesrc=" " alt="Verify Now" 
                style="max-width:136px; width:136px; outline:none!important; border:0" width="136"></a></td></tr>
                <tr><td class="x_em_padding x_td_width" style="font-family:'Work Sans',Arial,sans-serif; font-size:16px; font-style:normal; font-weight:400; line-height:26px; letter-spacing:0px; padding-left:60px; padding-right:60px" align="left">
                Or, paste this link into your browser: <a href=" " target="_blank" rel="noopener noreferrer" 
                data-auth="NotApplicable">{link}</a> </td></tr>
                <tr></tr>
                
                <tr><td class="x_em_padding x_td_width" style="font-family:'Work Sans',Arial,sans-serif; font-size:16px; font-style:normal; font-weight:400; line-height:26px; letter-spacing:0px; padding-top:16px; padding-left:60px; padding-right:60px" align="left">Thank you, <br>The SOMOS Vaccination Team </td></tr>
                <tr></tr><tr><td class="x_em_padding" style="font-family:'Work Sans',Arial,sans-serif; font-size:14px; font-style:normal; font-weight:400; line-height:24px; letter-spacing:0px; text-align:left; padding:24px 60px 120px 60px">
             
                </tr>
            </tbody>
            </table>"""
        return subject, content


if __name__ == '__main__':
    biz = EmailVerifySes()
    biz.send_verify_email(clinic_id=3553, patient_id=2232, email_addr='aaa168@126.com')
    # link = biz.generate_verify_link()

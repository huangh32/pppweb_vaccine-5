import datetime
from somos.employee_slot import SlotDataService
from somos.biz.slot_utils import SlotUtils
from PhmWeb.common import DateUtils, DictUtils
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire


def test_slot_locking_old_logic():
    clinic_id = 3553
    slot_service = SlotDataService(clinic_id=clinic_id)
    qid = 'f10b35cd-5330-11eb-80cf-005056c00008'
    # ques_sql = "select BookingLocationID, BookingPeriod from somos_CovidRegQuestionnaire where qid='{0}'".format(qid)
    ques_biz = IcCovidRegQuestionnaire()
    ques_info = ques_biz.select_booking_info(qid)
    is_available = slot_service.is_available_to_book(location=ques_info['BookingLocationID'], book_time=ques_info['BookingPeriod'], my_locker=qid)
    print(f'is_available={is_available}')


def test_slot_before_commit():
    dt = datetime.datetime.now()
    is_ok = SlotUtils.is_able_to_commit_appointment(location_id=1,book_time=dt)


if __name__ == '__main__':
    #test_slot_before_commit()
    book_time = datetime.datetime.now()
    ss = DateUtils.datetime_to_short_string(book_time)
    print(ss)
